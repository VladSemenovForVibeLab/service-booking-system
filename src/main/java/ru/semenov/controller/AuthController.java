package ru.semenov.controller;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.semenov.dto.*;
import ru.semenov.service.authentication.IAuthenticationService;
import ru.semenov.service.authentication.IUserEntityService;

import java.time.LocalDateTime;

@RestController
@RequestMapping(AuthController.AUTH_CONTROLLER_URL)
public class AuthController {
    private final IAuthenticationService iAuthenticationService;
    public static final String AUTH_CONTROLLER_URL = "/api/v1/authentication";

    public AuthController(IUserEntityService iUserEntityService, IAuthenticationService iAuthenticationService) {
        this.iAuthenticationService = iAuthenticationService;
    }
    @PostMapping("/register")
    public ResponseEntity<RegistrationResponse> register( @RequestBody UserEntityDTORequestForRegistration userEntityDTORequestForRegistration){
        iAuthenticationService.registerUserEntity(userEntityDTORequestForRegistration);
        return new ResponseEntity<>(new RegistrationResponse(HttpStatus.CREATED,"Пользователь успешно зарегестрирован!", LocalDateTime.now()),HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public LoginResponse login(@Validated @RequestBody LoginRequest loginRequest){
        return iAuthenticationService.login(loginRequest);
    }

    @GetMapping("/activate/{code}")
    public ResponseEntity<ActivateCodeResponse> activate(@Validated @PathVariable("code") String code){
        boolean isActivated = iAuthenticationService.activateUserEntity(code);
        if(isActivated){
            return new ResponseEntity<>(new ActivateCodeResponse(HttpStatus.OK,"Пользователь успешно активирован!",LocalDateTime.now()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new ActivateCodeResponse(HttpStatus.BAD_REQUEST,"Пользователь не активирован!",LocalDateTime.now()),HttpStatus.BAD_REQUEST);
    }
}
