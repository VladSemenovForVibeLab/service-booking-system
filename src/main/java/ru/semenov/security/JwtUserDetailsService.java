package ru.semenov.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.semenov.service.authentication.IUserEntityService;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class JwtUserDetailsService implements UserDetailsService {
    private final IUserEntityService userEntityService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userEntityService.getUserEntityByEmail(username);
    }
}
