package ru.semenov.security;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@ConfigurationProperties(prefix = "security.jwt")
public class JwtProperties {
    @Schema(description = "Секретная строка",example = "$2a$12$wYKEjSVwzMVnYTp.TEWvZuBATdmINrupWhYDSa83pWnO0fMtZoTYm")
    private String secret;
    @Schema(description = "Продолжительность жизни токена!",example = "7")
    private long access;
}
