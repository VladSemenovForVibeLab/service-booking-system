package ru.semenov.enums;
import org.springframework.security.core.GrantedAuthority;
public enum UserRole implements GrantedAuthority{
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_EXTERNAL,
    ROLE_SUPER_ADMIN,
    ROLE_SUPER_USER,
    ROLE_EMPLOYEE,
    ROLE_MANAGER,
    ;

    @Override
    public String getAuthority() {
        return name().toLowerCase();
    }
}
