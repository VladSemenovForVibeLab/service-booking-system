package ru.semenov.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import ru.semenov.entity.UserEntity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class UserEntityDTORequestForRegistration {
    @Schema(description = "почта для данного пользователя",example = "ooovladislavchik@gmail.com")
    private String email;
    @Schema(description = "Пароль для данного польщзователя",example = "1q2w3e")
    private String password;
    @Schema(description = "Имя пользователя",example = "Vlad")
    private String name;
    @Schema(description = "Фамилия пользователя",example = "Semenov")
    private String lastname;
    @Schema(description = "Мобильный телефон",example = "78987878998")
    private String phone;
    public UserEntityDTORequestForRegistration (UserEntity userEntity){
        this.email=userEntity.getEmail();
        this.password=userEntity.getPassword();
        this.name=userEntity.getName();
        this.lastname=userEntity.getLastname();
        this.phone=userEntity.getPhone();
    }
}
