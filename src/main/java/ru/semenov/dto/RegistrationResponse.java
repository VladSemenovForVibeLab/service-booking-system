package ru.semenov.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Schema(description = "Ответ на регистрацию пользователя!")
public class RegistrationResponse {
    @Schema(name = "статус в ответ на регистрацию",example = "200")
    private HttpStatus httpStatus;
    @Schema(name = "сообщение в ответ на регистрацию",example = "Данный пользователь успешно зарегестрирован!")
    private String message;
    @Schema(name = "дата регистрации")
    private LocalDateTime registrationDate;
}
