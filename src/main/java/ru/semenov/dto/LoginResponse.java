package ru.semenov.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@EqualsAndHashCode
@Schema(description = "Ответ при аутентификации")
public class LoginResponse {
    @Schema(description = "Почта пользователя", example = "ooovladislavchik@gmail.com")
    private String email;
    @Schema(description = "Токен аутентификации, авторизации",example = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdHJpbmcyIiwidXNlcl9tb2RlbF91aWQiOiI5N2Y5M2ZlMS0yOWM1LTRhY2MtYTQ5Mi03OTA1NGNhMTJkZjciLCJyb2xlcyI6WyJST0xFX1VTRVIiXSwiZXhwIjoxNzAzNjMyOTY3fQ.YRK_lR32Zl1-5CZokNWlSXMhQ5AnFybvDCrZt-sSqA20J8q6NEAhmf0kUHlfM74NcR-83rZSkpAhSFe1LH9QdA")
    private String accessToken;
}
