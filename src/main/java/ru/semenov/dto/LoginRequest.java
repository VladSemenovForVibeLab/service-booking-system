package ru.semenov.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Schema(description = "Запрос на аутентификацию")
public class LoginRequest {
    @Schema(description = "Почта пользователя",example = "ooovladislavchik@gmail.com")
    private String email;
    @Schema(description = "Пароль пользователя",example = "1q2w3e")
    private String password;
}
