package ru.semenov.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Schema(description = "Ответ на активацию аккаунта")
public class ActivateCodeResponse {
    private HttpStatus httpStatus;
    private String message;
    private LocalDateTime localDateTime;
}
