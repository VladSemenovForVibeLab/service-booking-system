package ru.semenov.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.semenov.enums.UserRole;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Builder
@Table(name = UserEntity.TABLE_NAME)
public class UserEntity implements Serializable,Comparable<UserEntity>, UserDetails {
    public static final String TABLE_NAME = "user_entity";
    private static final String USER_ENTITY_ID_COLUMN_NAME = "id";
    private static final String USER_ENTITY_DATE_CREATED_COLUMN_NAME = "date_created";
    private static final String USER_ENTITY_DATE_UPDATED_COLUMN_NAME = "date_updated";
    private static final String USER_ENTITY_EMAIL_COLUMN_NAME = "email";
    private static final String USER_ENTITY_PASSWORD_COLUMN_NAME = "password";
    private static final String USER_ENTITY_NAME_COLUMN_NAME = "name";
    private static final String USER_ENTITY_LASTNAME_COLUMN_NAME = "lastname";
    private static final String USER_ENTITY_PHONE_COLUMN_NAME = "phone";
    private static final String USER_ENTITY_ACTIVE_COLUMN_NAME = "active";
    private static final String USER_ENTITY_NOT_EXPIRED_COLUMN_NAME = "not_expired";
    private static final String USER_ENTITY_NOT_BLOCKED_COLUMN_NAME = "not_blocked";
    private static final String USER_ENTITY_ACCOUNT_NOT_EXPIRED_COLUMN_NAME ="account_not_expires";
    private static final String USER_ENTITY_ACTIVE_CODE_COLUMN_NAME = "active_code";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = USER_ENTITY_ID_COLUMN_NAME,nullable = false,unique = true)
    private String id;
    @Transient
    private Object lock;
    @Column(name = USER_ENTITY_DATE_CREATED_COLUMN_NAME,nullable = false)
    private LocalDateTime dateCreated;
    @Column(name = USER_ENTITY_DATE_UPDATED_COLUMN_NAME,nullable = false)
    private LocalDateTime dateUpdated;
    @Column(name = USER_ENTITY_EMAIL_COLUMN_NAME,nullable = false,unique = true,length = 1000)
    private String email;
    @Column(name = USER_ENTITY_PASSWORD_COLUMN_NAME,nullable = false,length = 10000)
    private String password;
    @Column(name = USER_ENTITY_NAME_COLUMN_NAME,nullable = false,length = 200)
    private String name;
    @Column(name = USER_ENTITY_LASTNAME_COLUMN_NAME,nullable = false,length = 200)
    private String lastname;
    @Column(name = USER_ENTITY_PHONE_COLUMN_NAME,nullable = false,length = 100)
    private String phone;
    @ElementCollection(targetClass = UserRole.class,fetch = FetchType.EAGER)
    @CollectionTable(name = "user_roles",joinColumns = @JoinColumn(name = "user_entity_id"))
    @Enumerated(EnumType.STRING)
    private Set<UserRole> roles = new HashSet<>();
    @Column(name = USER_ENTITY_ACTIVE_COLUMN_NAME,nullable = false)
    private boolean active = true;
    @Column(name = USER_ENTITY_NOT_EXPIRED_COLUMN_NAME,nullable = false)
    private boolean notExpired = true;
    @Column(name = USER_ENTITY_NOT_BLOCKED_COLUMN_NAME,nullable = false)
    private boolean notBlocked = true;
    @Column(name = USER_ENTITY_ACCOUNT_NOT_EXPIRED_COLUMN_NAME,nullable = false)
    private boolean accountNotExpired = true;
    @Column(name = USER_ENTITY_ACTIVE_CODE_COLUMN_NAME)
    private String activateCode;
    @PrePersist
    public void onCreate(){
        this.dateCreated = LocalDateTime.now();
        this.dateUpdated = LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        this.dateUpdated =LocalDateTime.now();
    }
    public void lock(){
        synchronized (lock){

        }
    }

    @Override
    public int compareTo(UserEntity o) {
        return this.email.compareTo(o.email);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNotExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return notBlocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return notExpired;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}
