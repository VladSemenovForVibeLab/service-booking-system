package ru.semenov.service.authentication;

import ru.semenov.dto.UserEntityDTORequestForRegistration;
import ru.semenov.entity.UserEntity;

public interface IUserEntityService {
    UserEntity getUserEntityByEmail(String email);

}
