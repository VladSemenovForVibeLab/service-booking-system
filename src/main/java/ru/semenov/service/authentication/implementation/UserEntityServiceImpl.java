package ru.semenov.service.authentication.implementation;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;
import ru.semenov.dto.UserEntityDTORequestForRegistration;
import ru.semenov.entity.UserEntity;
import ru.semenov.repository.UserEntityRepository;
import ru.semenov.service.authentication.IUserEntityService;

@Service
public class UserEntityServiceImpl implements IUserEntityService {
    private final AuthenticationManager authenticationManager;
   private final UserEntityRepository userEntityRepository;

    public UserEntityServiceImpl(AuthenticationManager authenticationManager, UserEntityRepository userEntityRepository) {
        this.authenticationManager = authenticationManager;
        this.userEntityRepository = userEntityRepository;
    }

    @Override
    public UserEntity getUserEntityByEmail(String email) {
        return userEntityRepository.findUserEntityByEmail(email);
    }

}
