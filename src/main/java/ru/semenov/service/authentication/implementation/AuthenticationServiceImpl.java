package ru.semenov.service.authentication.implementation;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.LoginRequest;
import ru.semenov.dto.LoginResponse;
import ru.semenov.dto.UserEntityDTORequestForRegistration;
import ru.semenov.entity.UserEntity;
import ru.semenov.enums.UserRole;
import ru.semenov.exception.ResourceNotFoundException;
import ru.semenov.exception.UserAlreadyExistsException;
import ru.semenov.repository.UserEntityRepository;
import ru.semenov.security.JwtTokenProvider;
import ru.semenov.service.authentication.IAuthenticationService;
import ru.semenov.service.authentication.IMailSender;

import java.util.UUID;

@Service
@Slf4j
public class AuthenticationServiceImpl implements IAuthenticationService {
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final UserEntityRepository userEntityRepository;
    private final IMailSender mailSender;

    public AuthenticationServiceImpl(JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, UserEntityRepository userEntityRepository, IMailSender mailSender) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.userEntityRepository = userEntityRepository;
        this.mailSender = mailSender;
    }

    @Override
    @Transactional
    public void registerUserEntity(UserEntityDTORequestForRegistration userEntityDTORequestForRegistration) {
        if(userEntityRepository.findUserEntityByEmail(userEntityDTORequestForRegistration.getEmail())!=null){
            log.error("Пользователь с данной почтой {} уже зарегестрирован",userEntityDTORequestForRegistration.getEmail());
            throw new UserAlreadyExistsException("Пользователь с данной почтой уже зарегестрирован на портале!");
        }
        toUserEntityForRegister(userEntityDTORequestForRegistration);
    }

    @Override
    @Transactional
    public LoginResponse login(LoginRequest loginRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),loginRequest.getPassword()));
        UserEntity userEntity = userEntityRepository.findUserEntityByEmail(loginRequest.getEmail());
        return LoginResponse
                .builder()
                .email(userEntity.getEmail())
                .accessToken(jwtTokenProvider.createAccessToken(userEntity.getId(),userEntity.getEmail(),userEntity.getRoles()))
                .build();
    }

    @Override
    @Transactional
    public boolean activateUserEntity(String code) {
        UserEntity userEntity = userEntityRepository.findUserEntityByActivateCode(code)
                .orElseThrow(()->new ResourceNotFoundException("Пользователь с данным кодом не найден!"));
        userEntity.setActive(true);
        userEntity.setActivateCode(null);
        userEntityRepository.save(userEntity);
        return true;
    }

    private void toUserEntityForRegister(UserEntityDTORequestForRegistration userEntityDTORequestForRegistration) {
        UserEntity userEntityForRegistration = UserEntity
                .builder()
                .email(userEntityDTORequestForRegistration.getEmail())
                .password(passwordEncoder.encode(userEntityDTORequestForRegistration.getPassword()))
                .name(userEntityDTORequestForRegistration.getName())
                .lastname(userEntityDTORequestForRegistration.getLastname())
                .phone(userEntityDTORequestForRegistration.getPhone())
                .active(false)
                .activateCode(UUID.randomUUID().toString())
                .build();
        userEntityForRegistration.getRoles().add(UserRole.ROLE_USER);
        userEntityRepository.save(userEntityForRegistration);
        sendMessageEmail(userEntityForRegistration);
    }

    private void sendMessageEmail(UserEntity userEntityForRegistration) {
        if(!StringUtils.isEmpty(userEntityForRegistration.getEmail())){
            String message = String.format(
                    "Добрый день, %s %s \n"+
                            "Добро пожаловать на портал по бронированию номеров! \n"+
                            "Чтобы продолжить пользоваться данным порталом перейдите по ссылке -> http://localhost:8080/api/v1/authentication/activate/%s",
                    userEntityForRegistration.getName(),
                    userEntityForRegistration.getLastname(),
                    userEntityForRegistration.getEmail()
            );
            mailSender.sendMail(userEntityForRegistration.getEmail(),"Activation code",message);
        }
    }
}
