package ru.semenov.service.authentication.implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.semenov.service.authentication.IMailSender;

@Service
public class IMailSenderImpl implements IMailSender {
    private final JavaMailSender javaMailSender;
    @Value("${spring.mail.username")
    private String username;

    public IMailSenderImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendMail(String email, String activationCode, String message) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(username);
        simpleMailMessage.setTo(email);
        simpleMailMessage.setSubject(activationCode);
        simpleMailMessage.setText(message);
        javaMailSender.send(simpleMailMessage);
    }
}
