package ru.semenov.service.authentication;

public interface IMailSender {
    public void sendMail(String email, String activationCode, String message) ;
}
