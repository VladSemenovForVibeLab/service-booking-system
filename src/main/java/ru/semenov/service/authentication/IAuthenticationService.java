package ru.semenov.service.authentication;

import ru.semenov.dto.LoginRequest;
import ru.semenov.dto.LoginResponse;
import ru.semenov.dto.UserEntityDTORequestForRegistration;

public interface IAuthenticationService {
    void registerUserEntity(UserEntityDTORequestForRegistration userEntityDTORequestForRegistration);

    LoginResponse login(LoginRequest loginRequest);

    boolean activateUserEntity(String code);
}
