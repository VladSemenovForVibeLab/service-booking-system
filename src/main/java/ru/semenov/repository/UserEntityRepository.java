package ru.semenov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.semenov.entity.UserEntity;

import java.util.Optional;

@Repository
public interface UserEntityRepository extends JpaRepository<UserEntity, String> {
    UserEntity findUserEntityByEmail(String email);

    Optional<UserEntity> findUserEntityByActivateCode(String code);
}